package com.popup
{
	import com.abstract.Screen;
	import com.data.PopupName;
	import com.data.ScreenName;
	import com.utils.ImagePickHelper;
	
	import flash.display.BitmapData;
	import flash.events.MouseEvent;
	
	public class ChoosePhotoPopup extends Screen
	{
		private var _content:ChoosePhotoPopupFLA;
		
		public function ChoosePhotoPopup():void
		{
			_content = new ChoosePhotoPopupFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_content.btnClose.addEventListener(MouseEvent.CLICK, onCloseClick);
			_content.btnTakePhoto.addEventListener(MouseEvent.CLICK, onTakePhotoClick);
			_content.btnGallery.addEventListener(MouseEvent.CLICK, onGalleryClick);
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_content.btnClose.removeEventListener(MouseEvent.CLICK, onCloseClick);
			_content.btnTakePhoto.removeEventListener(MouseEvent.CLICK, onTakePhotoClick);
			_content.btnGallery.removeEventListener(MouseEvent.CLICK, onGalleryClick);
		}
		
		protected function onCloseClick(e:MouseEvent):void
		{
			app.screen.hidePopup();
		}
		
		protected function onTakePhotoClick(e:MouseEvent):void
		{
			app.screen.showPopup(PopupName.LOADING_POPUP);
			ImagePickHelper.takePhoto(onImageGot);
		}
		
		protected function onGalleryClick(e:MouseEvent):void
		{
			app.screen.showPopup(PopupName.LOADING_POPUP);
			ImagePickHelper.pickImage(onImageGot);
		}
		
		private function onImageGot(image:BitmapData):void
		{
			app.screen.hidePopup();
			
			if (!image)
				return;
			
			app.data.setFingerImage(image);
			app.screen.showScreen(ScreenName.SET_PHOTO_SCREEN);		
		}
	}
}