package com.popup
{
	import com.abstract.Screen;
	import com.data.PopupName;
	import com.utils.SocialMediaHelper;
	
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.utils.ByteArray;
	
	public class ReadyVideoPopup extends Screen
	{
		private var _content:ReadyVideoPopupFLA;
		private var _socialHelper:SocialMediaHelper;
		
		public function ReadyVideoPopup():void
		{
			_content = new ReadyVideoPopupFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_socialHelper = new SocialMediaHelper();
			
			_content.btnGallery.addEventListener(MouseEvent.CLICK, onGalleryClick);
			_content.btnFacebook.addEventListener(MouseEvent.CLICK, onFacebookClick);
			_content.btnInstagram.addEventListener(MouseEvent.CLICK, onInstagramClick);
			_content.btnOK.addEventListener(MouseEvent.CLICK, onOKClick);
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_content.btnGallery.removeEventListener(MouseEvent.CLICK, onGalleryClick);
			_content.btnFacebook.removeEventListener(MouseEvent.CLICK, onFacebookClick);
			_content.btnInstagram.removeEventListener(MouseEvent.CLICK, onInstagramClick);
			_content.btnOK.removeEventListener(MouseEvent.CLICK, onOKClick);
		}
		
		protected function onGalleryClick(e:MouseEvent):void
		{
			disableButton(_content.btnGallery);
			app.videoHelper.saveVideoToGallery(onVideoSaved);
			//AirImagePicker.getInstance().displayImagePicker(onVideoPicked, true);
		}
		
		protected function onFacebookClick(e:MouseEvent):void
		{
			if (!app.data.curSavedVideoURL)
				return;
			
			disableButton(_content.btnFacebook);
			_socialHelper.shareFacebook(app.data.curSavedVideoURL);
		}
		
		protected function onInstagramClick(e:MouseEvent):void
		{
			if (!app.data.curSavedVideoURL)
				return;
			
			disableButton(_content.btnInstagram);
			_socialHelper.shareInstagram(app.data.curSavedVideoURL);
		}
		
		protected function onOKClick(e:MouseEvent):void
		{
			app.screen.hidePopup();
		}
		
		private function onVideoPicked(status:String, url:String = "", data:BitmapData = null, bytes:ByteArray = null):void
		{
			trace("onVideoPicked: ", status, url);
		}
		
		private function onVideoSaved():void
		{
			trace("onVideoSavedToGallery");
		}
		
		private function disableButton(btn:MovieClip):void
		{
			btn.mouseEnabled = false;
			btn.alpha = 0.5;
		}
	}
}