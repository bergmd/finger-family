package com.manager
{
	import flash.display.BitmapData;
	import flash.utils.ByteArray;

	public class DataManager
	{
		private var _curFingerNum:int = 1;
		private var _curFingerImage:BitmapData;
		private var _curBackgroundNum:int;
		private var _curSavedVideoURL:String;
		
		private var _familyMemberTypes:Array = [0, 0, 0, 0, 0];
		private var _familyMemberImages:Array = [null, null, null, null, null];
		private var _familyMemberVoices:Array = [null, null, null, null, null];
		
		public function DataManager()
		{
		}

		public function setFingerImage(value:BitmapData):void
		{
			_curFingerImage = value;
			_familyMemberImages[curFingerNum - 1] = value;
		}
		
		public function getFingerImage(fingerNum:int):BitmapData
		{
			if (_familyMemberImages[fingerNum])
				return _familyMemberImages[fingerNum];
			
			return null;
		}
		
		public function setFingerNum(value:int):void
		{
			_curFingerNum = value;
		}
		
		public function setFamilyMember(value:int):void
		{
			_familyMemberTypes[curFingerNum - 1] = value;
		}
		
		public function getFamilyMember(fingerNum:int):int
		{
			if (_familyMemberTypes[fingerNum - 1])
				return _familyMemberTypes[fingerNum - 1];
			
			return 1;
		}
		
		public function setFamilyMemberVoice(value:ByteArray):void
		{
			_familyMemberVoices[curFingerNum - 1] = value;
		}
		
		public function setBackground(value:int):void
		{
			_curBackgroundNum = value;
		}
		
		public function set setSavedVideoURL(value:String):void
		{
			_curSavedVideoURL = value;
		}
		
		// get/set
		public function get curFingerNum():int { return _curFingerNum; }
		public function get curFingerImage():BitmapData { return _curFingerImage; }
		public function get curFamilyMember():int { return _familyMemberTypes[curFingerNum - 1]; }
		public function get curFamilyMemberVoice():ByteArray { return _familyMemberVoices[curFingerNum - 1]; }
		public function get curBackgroundNum():int { return _curBackgroundNum; }
		public function get curSavedVideoURL():String { return _curSavedVideoURL; }
		public function get isFingersSet():Boolean
		{
			var result:Boolean = true;
			for each (var image:BitmapData in _familyMemberImages)
			{
				if (!image)
				{
					result = false;
					break;
				}
			}
			return result;
		}
		
		public function get isVoicesSet():Boolean
		{
			var result:Boolean = true;
			for each (var audio:ByteArray in _familyMemberVoices)
			{
				if (!audio)
				{
					result = false;
					break;
				}
			}
			return result;
		}
		
		public function get isBackgroundSet():Boolean
		{
			return _curBackgroundNum != 0;
		}
	}
}