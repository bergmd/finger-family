package com.manager
{
	import com.abstract.IScreen;
	import com.data.PopupName;
	
	import flash.display.Shape;
	import flash.display.Sprite;

	public class ScreenManager
	{
		private var _container:Sprite;
		private var _popupContainer:Sprite;
		private var _curScreen:IScreen;
		private var _curPopup:IScreen;
		
		public function ScreenManager(container:Sprite)
		{
			_container = container;
			
			var popupBG:Shape = new Shape();
			popupBG.graphics.clear() ;
			popupBG.graphics.beginFill(0x000000, 0.5);
			popupBG.graphics.drawRect(-AppSizeManager.DEFAULT_WIDTH * 0.5, 0, AppSizeManager.DEFAULT_WIDTH * 2, AppSizeManager.DEFAULT_HEIGHT);
			popupBG.graphics.endFill();
			_popupContainer = new Sprite();
			_popupContainer.addChild(popupBG);
		}
		
		public function showScreen(screenClass:Class):void
		{
			if (_curScreen == screenClass)
				throw Error("Screen " + screenClass + " is shown!");
			
			if (_curScreen != null)
			{
				_curScreen.dispose();
				_container.removeChild(_curScreen.content);
				_curScreen = null;
			}
			
			_curScreen = new screenClass();
			_curScreen.init();
			_container.addChild(_curScreen.content);
		}
		
		public function showPopup(popupClass:Class):void
		{
			if (_curPopup == popupClass)
				throw Error("Popup " + popupClass + " is shown!");
			
			hidePopup();
			_curPopup = new popupClass();
			_curPopup.init();
			if (popupClass == PopupName.LOADING_POPUP)
			{
				_curPopup.content.x = AppSizeManager.DEFAULT_WIDTH * 0.5;
				_curPopup.content.y = AppSizeManager.DEFAULT_HEIGHT * 0.5;
			}
			else 
			{
				_curPopup.content.x = AppSizeManager.DEFAULT_WIDTH * 0.5 - _curPopup.content.width * 0.5;
				_curPopup.content.y = AppSizeManager.DEFAULT_HEIGHT * 0.5 - _curPopup.content.height * 0.5;
			}
			_popupContainer.addChild(_curPopup.content);
			_container.addChild(_popupContainer);
		}
		
		public function hidePopup():void
		{
			if (_curPopup != null)
			{
				_curPopup.dispose();
				_popupContainer.removeChild(_curPopup.content);
				_container.removeChild(_popupContainer);
				_curPopup = null;
			}
		}
		
		public function hideLoadingPopup():void
		{
			if (_curPopup != null)
			{
				_curPopup.dispose();
				_popupContainer.removeChild(_curPopup.content);
				_container.removeChild(_popupContainer);
				_curPopup = null;
			}
		}
		
		// get/set
		public function get curScreen():IScreen { return _curScreen; }
		public function get container():Sprite { return _container; }
	}
}