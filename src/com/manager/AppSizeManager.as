package com.manager
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageOrientation;
	import flash.display.StageScaleMode;

	public class AppSizeManager
	{
		public static const DEFAULT_WIDTH:int = 697;
		public static const DEFAULT_HEIGHT:int = 1239;
		
		private var _container:Sprite;
		private var _defaultScale:Number;
		private var _defaultShiftX:uint;
		private var _defaultShiftY:uint;
		
		public function AppSizeManager(container:Sprite)
		{
			_container = container;
			
			_container.stage.scaleMode = StageScaleMode.NO_SCALE;
			_container.stage.align = StageAlign.TOP_LEFT;
			_container.stage.color = 0xF5F8F8;
			
			setPortraiteSize();
		}
		
		public function setPortraiteSize():void
		{
			_container.stage.setOrientation(StageOrientation.DEFAULT);
			
			if (!_defaultScale)
			{
				var fWidth:uint = _container.stage.fullScreenWidth;
				var fHeight:uint = _container.stage.fullScreenHeight;
				var xScale:Number = fWidth / DEFAULT_WIDTH;
				var yScale:Number = fHeight / DEFAULT_HEIGHT;
				var appScale:Number = (xScale < yScale) ? xScale : yScale;
				
				_defaultShiftX = (fWidth - (DEFAULT_WIDTH * appScale)) / 2;
				_defaultShiftY = (fHeight - (DEFAULT_HEIGHT * appScale)) / 2;
				_defaultScale = appScale;
			}
			
			_container.x = _defaultShiftX;
			_container.scaleX = _container.scaleY = _defaultScale;
		}
		
		public function setLandscapeSize():void
		{
			_container.stage.setOrientation(StageOrientation.ROTATED_RIGHT);
			
			var fWidth:uint = _container.stage.fullScreenWidth;
			var fHeight:uint = _container.stage.fullScreenHeight;
			var xScale:Number = fHeight / AppSizeManager.DEFAULT_WIDTH;
			var yScale:Number = fWidth / AppSizeManager.DEFAULT_HEIGHT;
			var appScale:Number = (xScale < yScale) ? xScale : yScale;
			
			_container.scaleX = _container.scaleY = appScale;
			_container.x = 0;
		}
	}
}