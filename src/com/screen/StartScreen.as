package com.screen
{
	import com.abstract.Screen;
	import com.data.ScreenName;
	
	import flash.events.MouseEvent;
	import flash.utils.setTimeout;
	
	public class StartScreen extends Screen
	{
		private var _content:StartScreenFLA;
		
		public function StartScreen():void
		{
			_content = new StartScreenFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_content.btnLogin.visible = false;
			_content.btnNewAccount.visible = false;
			setTimeout(showButtons, 1000);
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_content.btnLogin.removeEventListener(MouseEvent.CLICK, onLoginClick);
			_content.btnNewAccount.removeEventListener(MouseEvent.CLICK, onNewAccountClick);
		}
		
		private function showButtons():void
		{
			_content.btnLogin.visible = true;
			_content.btnNewAccount.visible = true;
			
			_content.btnLogin.addEventListener(MouseEvent.CLICK, onLoginClick);
			_content.btnNewAccount.addEventListener(MouseEvent.CLICK, onNewAccountClick);
		}
		
		protected function onLoginClick(e:MouseEvent):void
		{
			app.screen.showScreen(ScreenName.LOGIN_SCREEN);
		}
		
		protected function onNewAccountClick(e:MouseEvent):void
		{
			trace("onNewAccountClick");
		}
	}
}