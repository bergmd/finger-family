package com.screen
{
	import com.abstract.Screen;
	import com.data.ScreenName;
	import com.utils.AudioRecordHelper;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.utils.setTimeout;
	
	public class RecordVoiceScreen extends Screen
	{
		private static const BABY_FRAME_NUM:int = 3;
		private static const BABY_MASK_POSITION_X:int = 275;
		private static const OTHER_MASK_POSITION_X:int = 279;
		private static const MASK_POSITIONS_Y:Array = [196, 196, 279, 195, 193, 197, 197, 197, 197, 202, 202, 196, 196, 196];
		
		private var _content:RecordVoiceScreenFLA;
		private var _audioRecorder:AudioRecordHelper;
		private var _numMembers:int;
		private var _isRecording:Boolean;
		
		public function RecordVoiceScreen():void
		{
			_content = new RecordVoiceScreenFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_audioRecorder = new AudioRecordHelper();
			_numMembers = _content.mcIcon.totalFrames;
			
			_content.mcRunningText.gotoAndStop(1);
			_content.mcRunningText.mouseEnabled = _content.mcRunningText.mouseChildren = false;
			
			(app.data.curFamilyMember) ? setIconFrame(app.data.curFamilyMember) : setIconFrame(1);
			
			_content.abtnRecord.addEventListener(MouseEvent.CLICK, onRecordingStart);
			
			showMemberImage();
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_audioRecorder = null;
			
			_content.abtnRecord.removeEventListener(MouseEvent.CLICK, onRecordingStart);
		}
		
		private function showMemberImage():void
		{
			var image:Bitmap = new Bitmap(app.data.curFingerImage);
			image.smoothing = true;
			_content.mcImagePlace.addChild(image);
			
			if (_content.mcImagePlace.width < _content.mcImagePlace.height)
			{
				_content.mcImagePlace.width = _content.mcImageMask.width;
				_content.mcImagePlace.scaleY = _content.mcImagePlace.scaleX;
			}
			else 
			{
				_content.mcImagePlace.height = _content.mcImageMask.height;
				_content.mcImagePlace.scaleX = _content.mcImagePlace.scaleY;
			}
			_content.mcImagePlace.x = _content.mcImageMask.x;
			_content.mcImagePlace.y = _content.mcImageMask.y;
		}
		
		protected function onRecordingStart(e:MouseEvent):void
		{
			if (_isRecording)
				return;
			
			_isRecording = true;
			_content.abtnRecord.play();
			_content.mcRunningText.play();
			
			setTimeout(startRecording, 1800);
		}
		
		private function startRecording():void
		{
			_audioRecorder.startRecording();
			setTimeout(stopRecording, 6000);
		}
		
		private function stopRecording():void
		{
			if (!_isRecording)
				return;
			
			_isRecording = false;
			_audioRecorder.stopRecording();
			app.data.setFamilyMemberVoice(_audioRecorder.getRecording());
			
			setTimeout(showFingersScreen, 1000);
			
			//if (app.data.curFamilyMemberVoice)
			//	_audioRecorder.playRecording(app.data.curFamilyMemberVoice);
		}
		
		private function showFingersScreen():void
		{
			app.screen.showScreen(ScreenName.SET_FINGERS_SCREEN);
		}
		
		private function setIconFrame(frame:int):void
		{
			_content.mcImageMask.x = (frame == 3) ? BABY_MASK_POSITION_X : OTHER_MASK_POSITION_X;
			_content.mcImageMask.y = MASK_POSITIONS_Y[frame - 1];
			_content.mcImagePlace.x = _content.mcImageMask.x;
			_content.mcImagePlace.y = _content.mcImageMask.y;
			
			_content.mcIcon.gotoAndStop(frame);
		}
	}
}