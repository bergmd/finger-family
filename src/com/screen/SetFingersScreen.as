package com.screen
{
	import com.abstract.Screen;
	import com.data.PopupName;
	import com.data.ScreenName;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	public class SetFingersScreen extends Screen
	{
		private static const NUM_FINGERS:int = 5;
		private static const MASK_SIZE:int = 85;
		
		private var _content:SetFingersScreenFLA;
		private var _fingerButtons:Array;
		private var _fingerMasks:Array;
		private var _fingerIcons:Array;
		private var _isDoneEnabled:Boolean;
		
		public function SetFingersScreen():void
		{
			_content = new SetFingersScreenFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_content.btnDone.visible = _isDoneEnabled;
			
			_content.btnLeft.addEventListener(MouseEvent.CLICK, onLeftClick);
			_content.btnRight.addEventListener(MouseEvent.CLICK, onRightClick);
			_content.btnDone.addEventListener(MouseEvent.CLICK, onDoneClick);
			
			_fingerButtons = [_content.mcFinger1, _content.mcFinger2, _content.mcFinger3, _content.mcFinger4, _content.mcFinger5];
			for each (var mcFinger:MovieClip in _fingerButtons)
			{
				mcFinger.addEventListener(MouseEvent.MOUSE_DOWN, onFingerClick);
			}
			_fingerMasks = [_content.mcFingerMask1, _content.mcFingerMask2, _content.mcFingerMask3, _content.mcFingerMask4, _content.mcFingerMask5];
			for each (var mcFingerMask:MovieClip in _fingerMasks)
			{
				mcFingerMask.alpha = 0;
				mcFingerMask.mouseEnabled = false;
			}
			_fingerIcons = [_content.mcFingerIcon1, _content.mcFingerIcon2, _content.mcFingerIcon3, _content.mcFingerIcon4, _content.mcFingerIcon5];
			for each (var mcFingerIcon:MovieClip in _fingerIcons)
			{
				mcFingerIcon.visible = false;
				mcFingerIcon.mouseEnabled = false;
			}
			
			showFingerImages();
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_content.btnLeft.removeEventListener(MouseEvent.CLICK, onLeftClick);
			_content.btnRight.removeEventListener(MouseEvent.CLICK, onRightClick);
			_content.btnDone.removeEventListener(MouseEvent.CLICK, onDoneClick);
			
			for each (var mcFinger:MovieClip in _fingerButtons)
			{
				mcFinger.removeEventListener(MouseEvent.MOUSE_DOWN, onFingerClick);
			}
			_fingerButtons = [];
			_fingerMasks = [];
			_fingerIcons = [];
		}
		
		private function showFingerImages():void
		{
			var numImages:int;
			for (var i:int = 0; i < NUM_FINGERS; i++)
			{
				var bd:BitmapData = app.data.getFingerImage(i);
				if (bd)
				{
					numImages++;
					showFingerImage(bd, i);
				}
			}
			
			if (numImages == NUM_FINGERS)
			{
				_isDoneEnabled = true;
				_content.btnDone.visible = _isDoneEnabled;
			}
		}
		
		private function showFingerImage(bd:BitmapData, index:int):void
		{
			var curMask:MovieClip = _fingerMasks[index];
			_fingerButtons[index].alpha = 0;
			
			var imageContainer:Sprite = new Sprite();
			var image:Bitmap = new Bitmap(bd);
			image.x = -image.width * 0.5;
			image.y = -image.height * 0.5;
			image.smoothing = true;
			imageContainer.mouseEnabled = imageContainer.mouseChildren = false;
			imageContainer.addChild(image);
			
			if (imageContainer.width < imageContainer.height)
			{
				imageContainer.width = MASK_SIZE;
				imageContainer.scaleY = imageContainer.scaleX;
			}
			else 
			{
				imageContainer.height = MASK_SIZE;
				imageContainer.scaleX = imageContainer.scaleY;
			}
			imageContainer.rotation = curMask.rotation;
			imageContainer.x = curMask.x;
			imageContainer.y = curMask.y;
			imageContainer.mask = curMask;
			_content.addChild(imageContainer);
			
			var curIcon:MovieClip = _fingerIcons[index];
			curIcon.gotoAndStop(app.data.getFamilyMember(index + 1));
			curIcon.visible = true;
		}
		
		protected function onLeftClick(e:MouseEvent):void
		{
			var frame:int = _content.mcHand.currentFrame - 1;
			if (frame < 1)
				frame = 3;
			_content.mcHand.gotoAndStop(frame);
		}
		
		protected function onRightClick(e:MouseEvent):void
		{
			var frame:int = _content.mcHand.currentFrame + 1;
			if (frame > 3)
				frame = 1;
			_content.mcHand.gotoAndStop(frame);
		}
		
		protected function onFingerClick(e:MouseEvent):void
		{
			for each (var mcFinger:MovieClip in _fingerButtons)
			{
				mcFinger.gotoAndStop(1);
			}
			MovieClip(e.target).gotoAndStop(1);
			
			var targetMc:MovieClip = MovieClip(e.target);
			app.data.setFingerNum(int(targetMc.name.substr(targetMc.name.length - 1)));
			app.screen.showPopup(PopupName.CHOOSE_PHOTO_POPUP);
		}
		
		protected function onDoneClick(e:MouseEvent):void
		{
			app.screen.showScreen(ScreenName.VIDEO_MENU_SCREEN);
		}
	}
}