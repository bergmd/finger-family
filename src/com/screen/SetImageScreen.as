package com.screen
{
	import com.abstract.Screen;
	import com.data.ScreenName;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.TransformGestureEvent;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import flash.utils.setTimeout;
	
	public class SetImageScreen extends Screen
	{
		private var _content:SetImageScreenFLA;
		private var _imageContainer:Sprite;
		private var _dragBounds:Rectangle;
		private var _startDragPosX:int;
		private var _startDragPosY:int;
		private var _isDragging:Boolean;
		
		public function SetImageScreen():void
		{
			_content = new SetImageScreenFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_content.mcCross.mouseEnabled = _content.mcCross.mouseChildren = false;
			_content.mcImageMask.mouseEnabled = _content.mcImageMask.mouseChildren = false;
			_content.mcImagePlace.x = _content.mcCross.x;
			_content.mcImagePlace.y = _content.mcCross.y;
			
			Multitouch.inputMode = MultitouchInputMode.GESTURE;
			
			if (app.data.curFingerImage)
			{
				var image:Bitmap = new Bitmap(app.data.curFingerImage);
				_imageContainer = new Sprite();
				var halfWidth:int = image.width * 0.5;
				var halfHeight:int = image.height * 0.5;
				image.smoothing = true;
				image.x -= halfWidth;
				image.y -= halfHeight;
				_imageContainer.addChild(image);
				_content.mcImagePlace.x = _content.mcImageMask.x + halfWidth;
				_content.mcImagePlace.y = _content.mcImageMask.y + halfHeight;
				_content.mcImagePlace.addChild(_imageContainer);
				
				_dragBounds = new Rectangle(_content.mcImageMask.x + halfWidth, _content.mcImageMask.y + halfHeight, _content.mcImageMask.width - image.width, _content.mcImageMask.height - image.height);
				
				_content.mcImagePlace.addEventListener(MouseEvent.MOUSE_DOWN, onStartDragImage);
				_content.mcImagePlace.addEventListener(MouseEvent.MOUSE_OUT, onStopDragImage);
				_content.addEventListener(MouseEvent.MOUSE_UP, onStopDragImage);
				
				_content.mcImagePlace.addEventListener(TransformGestureEvent.GESTURE_PAN, onPanImage);
				app.screen.container.stage.addEventListener(TransformGestureEvent.GESTURE_ZOOM, onZoomImage);
			}
			
			_content.btnDone.addEventListener(MouseEvent.MOUSE_DOWN, onDonePress);
			_content.btnDone.addEventListener(MouseEvent.CLICK, onDoneClick);
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			if (_content.mcImagePlace.hasEventListener(MouseEvent.MOUSE_DOWN))
			{
				_content.mcImagePlace.removeEventListener(MouseEvent.MOUSE_DOWN, onStartDragImage);
				_content.mcImagePlace.removeEventListener(MouseEvent.MOUSE_OUT, onStopDragImage);
				_content.removeEventListener(MouseEvent.MOUSE_UP, onStopDragImage);
				
				_content.mcImagePlace.removeEventListener(TransformGestureEvent.GESTURE_PAN, onPanImage);
				app.screen.container.stage.removeEventListener(TransformGestureEvent.GESTURE_ZOOM, onZoomImage);
			}
			
			_content.btnDone.removeEventListener(MouseEvent.MOUSE_DOWN, onDonePress);
			_content.btnDone.removeEventListener(MouseEvent.CLICK, onDoneClick);
		}
		
		protected function onStartDragImage(e:MouseEvent):void
		{
			_isDragging = true;
			_content.mcImagePlace.startDrag(false, _dragBounds);
		}
		
		protected function onStopDragImage(e:MouseEvent):void
		{
			_isDragging = false;
			_content.mcImagePlace.stopDrag();
		}
		
		protected function onPanImage(e:TransformGestureEvent):void
		{
			//_imageContainer.x += e.offsetX;
			//_imageContainer.y += e.offsetY;
		}
		
		protected function onZoomImage(e:TransformGestureEvent):void
		{
			var curScaleX:Number = _imageContainer.scaleX * e.scaleX;
			var curScaleY:Number = _imageContainer.scaleY * e.scaleY;
			
			if (_imageContainer.width * curScaleX < _content.mcImageMask.width || _imageContainer.height * curScaleY < _content.mcImageMask.height)
			{
				//_imageContainer.width = _content.mcImageMask.width / curScaleX;
				//_imageContainer.height = _content.mcImageMask.height / curScaleY;
				return;
			}
			
			_imageContainer.scaleX = curScaleX;
			_imageContainer.scaleY = curScaleY;
			
			/*if (_content.mcImagePlace.x > _content.mcImageMask.x)
				_content.mcImagePlace.x = _content.mcImageMask.x;
			if (_content.mcImagePlace.y > _content.mcImageMask.y)
				_content.mcImagePlace.y = _content.mcImageMask.y;*/
			
			var halfWidth:int = _content.mcImagePlace.width * 0.5;
			var halfHeight:int = _content.mcImagePlace.height * 0.5;
			
			_dragBounds = new Rectangle(_content.mcImageMask.x + halfWidth, _content.mcImageMask.y + halfHeight, _content.mcImageMask.width - _content.mcImagePlace.width, _content.mcImageMask.height - _content.mcImagePlace.height);
			
			_content.mcImagePlace.startDrag(false, _dragBounds);
			setTimeout(_content.mcImagePlace.stopDrag, 200);
		}
		
		protected function onDonePress(e:MouseEvent):void
		{
			e.stopPropagation();
		}
		
		protected function onDoneClick(e:MouseEvent):void
		{
			_content.btnDone.mouseEnabled = false;
			_content.mcCross.play();
			
			saveImage();
			setTimeout(showChoosePhotoScreen, 500);
		}
		
		private function saveImage():void
		{
			var bd:BitmapData = new BitmapData(_content.mcImageMask.width, _content.mcImageMask.height);
			var m:Matrix = new Matrix();
			m.translate(-_content.mcImageMask.x + _content.mcImagePlace.x, -_content.mcImageMask.y + _content.mcImagePlace.y);
			bd.draw(_content.mcImagePlace, m, null, null, null, true);
			app.data.setFingerImage(bd);
			
			/*var bitmap:Bitmap = new Bitmap(bd);
			bitmap.smoothing = true;
			bitmap.scaleX = bitmap.scaleY = 0.5;
			_content.addChild(bitmap);*/
		}
		
		private function showChoosePhotoScreen():void
		{
			_content.btnDone.mouseEnabled = true;
			app.screen.showScreen(ScreenName.SET_MEMBER_SCREEN);
		}
	}
}