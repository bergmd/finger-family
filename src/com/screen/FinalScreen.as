package com.screen
{
	import com.abstract.Screen;
	import com.data.PopupName;
	import com.data.ScreenName;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	
	public class FinalScreen extends Screen
	{
		private static const NUM_FINGERS:int = 5;
		private static const MASK_SIZE:int = 85;
		
		private var _content:FinalScreenFLA;
		private var _fingerMasks:Array;
		private var _fingerIcons:Array;
		
		public function FinalScreen():void
		{
			_content = new FinalScreenFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			app.sizeManager.setLandscapeSize();
			
			_content.mcHandAnim.stop();
			_content.mcBackground.gotoAndStop(app.data.curBackgroundNum);
			
			with (_content.mcHandAnim.mcHand)
			{
				_fingerMasks = [mcFingerMask1, mcFingerMask2, mcFingerMask3, mcFingerMask4, mcFingerMask5];
				for each (var mcFingerMask:MovieClip in _fingerMasks)
				{
					mcFingerMask.alpha = 0;
					mcFingerMask.mouseEnabled = false;
				}
				_fingerIcons = [mcFingerIcon1, mcFingerIcon2, mcFingerIcon3, mcFingerIcon4, mcFingerIcon5];
				for each (var mcFingerIcon:MovieClip in _fingerIcons)
				{
					mcFingerIcon.visible = false;
					mcFingerIcon.mouseEnabled = false;
				}
			}
			
			showFingerImages();
			recordVideo();
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_fingerMasks = [];
			_fingerIcons = [];
		}
		
		private function showFingerImages():void
		{
			var numImages:int;
			for (var i:int = 0; i < NUM_FINGERS; i++)
			{
				var bd:BitmapData = app.data.getFingerImage(i);
				if (bd)
				{
					numImages++;
					showFingerImage(bd, i);
				}
			}
		}
		
		private function showFingerImage(bd:BitmapData, index:int):void
		{
			var curMask:MovieClip = _fingerMasks[index];
			
			var imageContainer:Sprite = new Sprite();
			var image:Bitmap = new Bitmap(bd);
			image.x = -image.width * 0.5;
			image.y = -image.height * 0.5;
			image.smoothing = true;
			imageContainer.mouseEnabled = imageContainer.mouseChildren = false;
			imageContainer.addChild(image);
			
			if (imageContainer.width < imageContainer.height)
			{
				imageContainer.width = MASK_SIZE;
				imageContainer.scaleY = imageContainer.scaleX;
			}
			else 
			{
				imageContainer.height = MASK_SIZE;
				imageContainer.scaleX = imageContainer.scaleY;
			}
			imageContainer.rotation = curMask.rotation;
			imageContainer.x = curMask.x;
			imageContainer.y = curMask.y;
			imageContainer.mask = curMask;
			_content.mcHandAnim.mcHand.addChild(imageContainer);
			
			var curIcon:MovieClip = _fingerIcons[index];
			curIcon.gotoAndStop(app.data.getFamilyMember(index + 1));
			curIcon.visible = true;
		}
		
		private function recordVideo():void
		{
			app.videoHelper.init(_content.mcHandAnim, _content.mcHandAnim.totalFrames, onVideoStart, onVideoFinish);
			app.videoHelper.recordVideo();
		}
		
		private function onVideoStart():void
		{
			_content.mcHandAnim.play();
		}
		
		private function onVideoFinish():void
		{
			app.videoHelper.saveVideoToFile(onVideoSaved);
		}
		
		private function onVideoSaved(url:String):void
		{
			trace("onVideoSaved: ", url);
			app.data.setSavedVideoURL = url;
			
			app.screen.hidePopup();
			app.sizeManager.setPortraiteSize();
			
			app.screen.showScreen(ScreenName.VIDEO_MENU_SCREEN);
			app.screen.showPopup(PopupName.READY_VIDEO_POPUP);
		}
	}
}