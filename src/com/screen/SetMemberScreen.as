package com.screen
{
	import com.Application;
	import com.abstract.Screen;
	import com.data.ScreenName;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	
	public class SetMemberScreen extends Screen
	{
		private static const BABY_FRAME_NUM:int = 3;
		private static const BABY_MASK_POSITION_X:int = 248;
		private static const OTHER_MASK_POSITION_X:int = 254;
		private static const MASK_POSITIONS_Y:Array = [364, 364, 490, 361, 358, 364, 364, 364, 364, 373, 373, 364, 364, 364];
		
		private var _content:SetMemberScreenFLA;
		private var _numMembers:int;
		
		public function SetMemberScreen():void
		{
			_content = new SetMemberScreenFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_numMembers = _content.mcIcon.totalFrames;
			
			(app.data.curFamilyMember) ? setIconFrame(app.data.curFamilyMember) : setIconFrame(1);
			
			_content.btnLeft.addEventListener(MouseEvent.CLICK, onLeftClick);
			_content.btnRight.addEventListener(MouseEvent.CLICK, onRightClick);
			_content.btnDone.addEventListener(MouseEvent.CLICK, onDoneClick);
			
			showMemberImage();
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_content.btnLeft.removeEventListener(MouseEvent.CLICK, onLeftClick);
			_content.btnRight.removeEventListener(MouseEvent.CLICK, onRightClick);
			_content.btnDone.removeEventListener(MouseEvent.CLICK, onDoneClick);
		}
		
		private function showMemberImage():void
		{
			var image:Bitmap = new Bitmap(app.data.curFingerImage);
			image.smoothing = true;
			_content.mcImagePlace.addChild(image);
			
			if (_content.mcImagePlace.width < _content.mcImagePlace.height)
			{
				_content.mcImagePlace.width = _content.mcImageMask.width;
				_content.mcImagePlace.scaleY = _content.mcImagePlace.scaleX;
			}
			else 
			{
				_content.mcImagePlace.height = _content.mcImageMask.height;
				_content.mcImagePlace.scaleX = _content.mcImagePlace.scaleY;
			}
			_content.mcImagePlace.x = _content.mcImageMask.x;
			_content.mcImagePlace.y = _content.mcImageMask.y;
		}
		
		protected function onLeftClick(e:MouseEvent):void
		{
			var frame:int = _content.mcIcon.currentFrame - 1;
			if (frame < 1)
				frame = _numMembers;
			
			setIconFrame(frame);
		}
		
		protected function onRightClick(e:MouseEvent):void
		{
			var frame:int = _content.mcIcon.currentFrame + 1;
			if (frame > _numMembers)
				frame = 1;
			
			setIconFrame(frame);
		}
		
		protected function onDoneClick(e:MouseEvent):void
		{
			app.data.setFamilyMember(_content.mcIcon.currentFrame);
			
			if (Application.DEBUG_MODE)
				app.screen.showScreen(ScreenName.SET_FINGERS_SCREEN);
			else 
				app.screen.showScreen(ScreenName.RECORD_VOICE_SCREEN);
		}
		
		private function setIconFrame(frame:int):void
		{
			_content.mcImageMask.x = (frame == 3) ? BABY_MASK_POSITION_X : OTHER_MASK_POSITION_X;
			_content.mcImageMask.y = MASK_POSITIONS_Y[frame - 1];
			_content.mcImagePlace.x = _content.mcImageMask.x;
			_content.mcImagePlace.y = _content.mcImageMask.y;
			
			_content.mcIcon.gotoAndStop(frame);
		}
	}
}