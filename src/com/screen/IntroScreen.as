package com.screen
{
	import com.abstract.Screen;

	public class IntroScreen extends Screen
	{
		private var _content:IntroScreenFLA;
		
		public function IntroScreen():void
		{
			_content = new IntroScreenFLA();
			super(_content);
		}
	}
}