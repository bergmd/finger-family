package com.screen
{
	import com.Application;
	import com.abstract.Screen;
	import com.data.ScreenName;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	public class VideoMenuScreen extends Screen
	{
		private var _content:VideoMenuScreenFLA;
		private var _marks:Array;
		
		public function VideoMenuScreen():void
		{
			_content = new VideoMenuScreenFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_marks = [_content.mcMark1, _content.mcMark2, _content.mcMark3];
			for each (var mcMark:Sprite in _marks) 
			{
				mcMark.visible = false;
			}
			
			_content.btnCreate.mouseEnabled = false;
			
			_content.btnPhotos.addEventListener(MouseEvent.CLICK, onPhotosClick);
			_content.btnVoices.addEventListener(MouseEvent.CLICK, onVoicesClick);
			_content.btnBackgrounds.addEventListener(MouseEvent.CLICK, onBackgroundsClick);
			_content.btnCreate.addEventListener(MouseEvent.CLICK, onCreateClick);
			
			checkCreateEnabling();			
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_content.btnPhotos.removeEventListener(MouseEvent.CLICK, onPhotosClick);
			_content.btnVoices.removeEventListener(MouseEvent.CLICK, onVoicesClick);
			_content.btnBackgrounds.removeEventListener(MouseEvent.CLICK, onBackgroundsClick);
			_content.btnCreate.removeEventListener(MouseEvent.CLICK, onCreateClick);
		}
		
		protected function onPhotosClick(e:MouseEvent):void
		{
			app.screen.showScreen(ScreenName.SET_FINGERS_SCREEN);
		}
		
		protected function onVoicesClick(e:MouseEvent):void
		{
			//app.screen.showScreen(ScreenName.RECORD_VOICE_SCREEN);
		}
		
		protected function onBackgroundsClick(e:MouseEvent):void
		{
			app.screen.showScreen(ScreenName.SET_BACKGROUND_SCREEN);
		}
		
		protected function onCreateClick(e:MouseEvent):void
		{
			app.screen.showScreen(ScreenName.FINAL_SCREEN);
		}
		
		private function showMark(index:int):void
		{
			if (!_marks[index].visible)
				_marks[index].visible = true;
		}
		
		private function checkCreateEnabling():void
		{
			if (app.data.isFingersSet)
				showMark(0);
			
			if (app.data.isVoicesSet)
				showMark(1);
			
			if (app.data.isBackgroundSet)
				showMark(2);
			
			var isEnabled:Boolean = (Application.DEBUG_MODE) ? true : (app.data.isFingersSet && app.data.isVoicesSet && app.data.isBackgroundSet);
			_content.btnCreate.mouseEnabled = isEnabled;
			var frame:int = (isEnabled) ? 2 : 1;
			_content.btnCreate.gotoAndStop(frame);
		}
	}
}