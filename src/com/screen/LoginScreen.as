package com.screen
{
	import com.abstract.Screen;
	
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.setTimeout;
	import com.data.ScreenName;
	
	public class LoginScreen extends Screen
	{
		private const EMAIL_DEFAULT_TEXT:String = "Enter your email";
		private const PASSWORD_DEFAULT_TEXT:String = "Enter your password";
		
		private var _content:LoginScreenFLA;
		private var _defaultTextFormat:TextFormat;
		private var _fullTextFormat:TextFormat;
		
		public function LoginScreen():void
		{
			_content = new LoginScreenFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_content.btnContinue.visible = false;
			_content.btnBack.visible = false;
			setTimeout(showButtons, 1000);
			
			_content.txtEmail.text = "";
			_content.txtPassword.text = "";
			
			_defaultTextFormat = _content.txtEmail.defaultTextFormat;
			_fullTextFormat = _content.txtEmail.defaultTextFormat;
			_fullTextFormat.color = "0x000000";
			
			showDefaultValue(_content.txtEmail, EMAIL_DEFAULT_TEXT);
			showDefaultValue(_content.txtPassword, PASSWORD_DEFAULT_TEXT);
			
			_content.txtEmail.addEventListener(FocusEvent.FOCUS_IN, onEmailFocusIn);
			_content.txtEmail.addEventListener(FocusEvent.FOCUS_OUT, onEmailFocusOut);
			_content.txtPassword.addEventListener(FocusEvent.FOCUS_IN, onPasswordFocusIn);
			_content.txtPassword.addEventListener(FocusEvent.FOCUS_OUT, onPasswordFocusOut);
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_content.txtEmail.removeEventListener(FocusEvent.FOCUS_IN, onEmailFocusIn);
			_content.txtEmail.removeEventListener(FocusEvent.FOCUS_OUT, onEmailFocusOut);
			_content.txtPassword.removeEventListener(FocusEvent.FOCUS_IN, onPasswordFocusIn);
			_content.txtPassword.removeEventListener(FocusEvent.FOCUS_OUT, onPasswordFocusOut);
			
			_content.btnContinue.removeEventListener(MouseEvent.CLICK, onContinueClick);
			_content.btnBack.removeEventListener(MouseEvent.CLICK, onBackClick);
		}
		
		private function showButtons():void
		{
			_content.btnContinue.visible = true;
			_content.btnBack.visible = true;
			
			_content.btnContinue.addEventListener(MouseEvent.CLICK, onContinueClick);
			_content.btnBack.addEventListener(MouseEvent.CLICK, onBackClick);
		}
		
		protected function onEmailFocusIn(e:FocusEvent):void
		{
			hideDefaultValue(_content.txtEmail, EMAIL_DEFAULT_TEXT);
		}
				
		protected function onEmailFocusOut(e:FocusEvent):void
		{
			showDefaultValue(_content.txtEmail, EMAIL_DEFAULT_TEXT);
		}
		
		protected function onPasswordFocusIn(e:FocusEvent):void
		{
			hideDefaultValue(_content.txtPassword, PASSWORD_DEFAULT_TEXT, true);
		}
		
		protected function onPasswordFocusOut(e:FocusEvent):void
		{
			showDefaultValue(_content.txtPassword, PASSWORD_DEFAULT_TEXT);
		}
		
		private function showDefaultValue(txt:TextField, defaultValue:String):void
		{
			if (txt.text == "")
			{
				txt.defaultTextFormat = _defaultTextFormat;
				txt.text = defaultValue;
				txt.displayAsPassword = false;
			}
		}
		
		private function hideDefaultValue(txt:TextField, defaultValue:String, password:Boolean = false):void
		{
			if (txt.text == defaultValue)
			{
				txt.defaultTextFormat = _fullTextFormat;
				txt.text = "";
				txt.displayAsPassword = password;
			}
		}
				
		protected function onContinueClick(e:MouseEvent):void
		{
			app.screen.showScreen(ScreenName.MENU_SCREEN);
		}
		
		protected function onBackClick(e:MouseEvent):void
		{
			app.screen.showScreen(ScreenName.START_SCREEN);
		}
	}
}