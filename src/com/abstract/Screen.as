package com.abstract
{
	import com.Application;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;

	public class Screen implements IScreen
	{
		private static const MOUSE_OVER_COLOR:ColorTransform = new ColorTransform(1, 1, 1, 1, 0, -57, -255, 0);
		private static const MOUSE_OUT_COLOR:ColorTransform = new ColorTransform();
		
		private var _content:MovieClip;
		public function Screen(content:MovieClip)
		{
			_content = content;
		}
		
		public function init():void
		{
			initButtons();
		}
		
		public function dispose():void
		{
			disposeButtons();
		}
		
		private function initButtons():void
		{
			var numChildren:int = _content.numChildren;
			for (var i:int = 0; i < numChildren; i++)
			{
				var child:DisplayObject = _content.getChildAt(i);
				if (child.name.substr(0, 3) == "btn")
				{
					child.addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
					child.addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
					child.addEventListener(MouseEvent.MOUSE_DOWN, onMouseOver);
					child.addEventListener(MouseEvent.MOUSE_UP, onMouseOut);
				}
			}
		}
		
		private function disposeButtons():void
		{
			var numChildren:int = _content.numChildren;
			for (var i:int = 0; i < numChildren; i++)
			{
				var child:DisplayObject = _content.getChildAt(i);
				if (child.name.substr(0, 3) == "btn")
				{
					child.removeEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
					child.removeEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
					child.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseOver);
					child.removeEventListener(MouseEvent.MOUSE_UP, onMouseOut);
				}
			}
		}
		
		protected function onMouseOver(e:Event):void
		{
			if (e.currentTarget.mouseEnabled)
				e.currentTarget.transform.colorTransform = MOUSE_OVER_COLOR;
		}
		
		protected function onMouseOut(e:Event):void
		{
			if (e.currentTarget.mouseEnabled)
				e.currentTarget.transform.colorTransform = MOUSE_OUT_COLOR;
		}
		
		// get/set
		public function get content():MovieClip
		{
			return _content;
		}
		
		public function get app():Application
		{
			return Application.instance;
		}
	}
}