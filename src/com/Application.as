package com
{
	import com.data.PopupName;
	import com.data.ScreenName;
	import com.manager.AppSizeManager;
	import com.manager.DataManager;
	import com.manager.ScreenManager;
	import com.utils.VideoConvertionHelper;
	
	import flash.display.Sprite;
	import flash.utils.setTimeout;

	public class Application 
	{
		public static const DEBUG_MODE:Boolean = true;
		
		private static var _instance:Application;

		public static function get instance():Application
		{
			if (!_instance)
				_instance = new Application();
			
			return _instance;
		}
		
		private var _container:Sprite;
		private var _sizeManager:AppSizeManager;
		private var _dataManager:DataManager;
		private var _screenManager:ScreenManager;
		private var _videoHelper:VideoConvertionHelper;

		
		public function init(container:Sprite):void
		{
			_container = container;
			_sizeManager = new AppSizeManager(container);
			_dataManager = new DataManager();
			_screenManager = new ScreenManager(container);
			_videoHelper = new VideoConvertionHelper(container);
			
			showInitScreen();
		}
		
		private function showInitScreen():void
		{
			screen.showScreen(ScreenName.INTRO_SCREEN);
			setTimeout(showStartScreen, 1500);
		}
		
		private function showStartScreen():void
		{
			if (DEBUG_MODE)
				screen.showScreen(ScreenName.SET_FINGERS_SCREEN);
			else 
				screen.showScreen(ScreenName.START_SCREEN);
			
			//screen.showPopup(PopupName.READY_VIDEO_POPUP);
		}
		
		// get/set
		public function get container():Sprite { return _container; }
		public function get sizeManager():AppSizeManager { return _sizeManager; }
		public function get data():DataManager { return _dataManager; }
		public function get screen():ScreenManager { return _screenManager; }
		public function get videoHelper():VideoConvertionHelper { return _videoHelper; }
	}
}