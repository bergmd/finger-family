package com.data
{
	import com.popup.ChoosePhotoPopup;
	import com.popup.LoadingPopup;
	import com.popup.NoVideosPopup;
	import com.popup.ReadyVideoPopup;

	public class PopupName
	{
		public static const LOADING_POPUP:Class = LoadingPopup;
		public static const NO_VIDEOS_POPUP:Class = NoVideosPopup;
		public static const READY_VIDEO_POPUP:Class = ReadyVideoPopup;
		public static const CHOOSE_PHOTO_POPUP:Class = ChoosePhotoPopup;
	}
}