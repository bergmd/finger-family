package com.data
{
	import com.screen.FinalScreen;
	import com.screen.IntroScreen;
	import com.screen.LoginScreen;
	import com.screen.MenuScreen;
	import com.screen.RecordVoiceScreen;
	import com.screen.SetBackgroundScreen;
	import com.screen.SetFingersScreen;
	import com.screen.SetImageScreen;
	import com.screen.SetMemberScreen;
	import com.screen.StartScreen;
	import com.screen.VideoMenuScreen;

	public class ScreenName
	{
		public static const INTRO_SCREEN:Class = IntroScreen;
		public static const START_SCREEN:Class = StartScreen;
		public static const LOGIN_SCREEN:Class = LoginScreen;
		public static const MENU_SCREEN:Class = MenuScreen;
		public static const VIDEO_MENU_SCREEN:Class = VideoMenuScreen;
		public static const SET_FINGERS_SCREEN:Class = SetFingersScreen;
		public static const SET_PHOTO_SCREEN:Class = SetImageScreen;
		public static const SET_BACKGROUND_SCREEN:Class = SetBackgroundScreen;
		public static const SET_MEMBER_SCREEN:Class = SetMemberScreen;
		public static const RECORD_VOICE_SCREEN:Class = RecordVoiceScreen;
		public static const FINAL_SCREEN:Class = FinalScreen;
	}
}