package com.utils
{
	import com.freshplanet.ane.AirImagePicker.AirImagePicker;
	
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.utils.ByteArray;

	public class ImagePickHelper
	{
		private static var _callback:Function;
		
		public function ImagePickHelper():void
		{
			AirImagePicker.getInstance().logEnabled = false;
		}
		
		public static function pickImage(callback:Function, cropping:Boolean = false):void
		{
			if (!AirImagePicker.getInstance().isImagePickerAvailable()){
				_callback.call();
				return;
			}
			
			_callback = callback;
			AirImagePicker.getInstance().displayImagePicker(onImagePicked);
		}
		
		public static function takePhoto(callback:Function, cropping:Boolean = false):void
		{
			if (!AirImagePicker.getInstance().isCameraAvailable()){
				_callback.call();
				return;
			}
			
			_callback = callback;
			AirImagePicker.getInstance().displayCamera(onImagePicked);
		}
		
		private static function onImagePicked(status:String, image:BitmapData = null, data:ByteArray = null):void
		{
			trace("[AirImagePicker Test] ANE returned status - ", status);
			
			if (image)
				trace("[AirImagePicker Test] ANE returned image of " + image.width + "x" + image.height);
			
			if (data)
			{
				trace("[AirImagePicker Test] ANE returned JPEG data of " + data.length + " bytes");
				loadImage(data);
			}
			else 
			{
				_callback.call(null, null);
			}
		}
		
		private static function loadImage(data:ByteArray):void
		{
			var loader:Loader = new Loader();
			loader.loadBytes(data);
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, loaderComplete);
			
			function loaderComplete(event:Event):void
			{
				var loaderInfo:LoaderInfo = LoaderInfo(event.target);
				var bitmapData:BitmapData = new BitmapData(loaderInfo.width, loaderInfo.height, false, 0xFFFFFF);
				bitmapData.draw(loaderInfo.loader);
				
				_callback.call(null, bitmapData);
			}
		}
	}
}