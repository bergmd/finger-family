package com.utils
{
	import com.empath.extensions.social.EmpathVideoShareExtension;
	
	import flash.filesystem.File;

	public class SocialMediaHelper
	{
		private static const DEFAULT_VIDEO_URL:String = "videos/Test_Video.mp4";
		
		private var _videoShareExt:EmpathVideoShareExtension;
		private var _videoFile:File;
		
		public function SocialMediaHelper()
		{
			_videoShareExt = new EmpathVideoShareExtension();
			
//			videoShareExt.composeSMS("Test Message","",[videoFile.nativePath+"|native|video/mp4|Video_Test"]);
//			videoShareExt.composeMail("Hello", "<html><body><p>Hi,</p></br><p>This is a test message.</body></html>","test@email.com",[videoFile.nativePath+"|native|mp4|Video_Test"]);
//			videoShareExt.composeFacebook("Test FB",[videoFile.nativePath+"|native|video/mp4|Video_Test"]);
//			videoShareExt.composeMessenger("Test FB",[videoFile.nativePath+"|native|video/mp4|Video_Test"]);
//			videoShareExt.saveVideoToCameraRoll([videoFile.nativePath+"|native|video/mp4|Video_Test"]);
//			videoShareExt.composeInstagram([videoFile.nativePath+"|native|video/mp4|Video_Test"]);
		}
		
		public function saveToCameraRoll(url:String):void
		{
			trace("saveToCameraRoll");
			_videoFile = File.applicationStorageDirectory.resolvePath(url);
			_videoShareExt.saveVideoToCameraRoll([_videoFile.nativePath + "|native|video/mp4|Video_Test"]);
		}
		
		public function shareFacebook(url:String):void
		{
			trace("shareFacebook");
			_videoFile = File.applicationStorageDirectory.resolvePath(url);
			_videoShareExt.composeFacebook("Test FB", [_videoFile.nativePath + "|native|video/mp4|Video_Test"]);
		}
		
		public function shareInstagram(url:String):void
		{
			trace("shareInstagram");
			_videoFile = File.applicationStorageDirectory.resolvePath(url);
			_videoShareExt.composeInstagram([_videoFile.nativePath + "|native|video/mp4|Video_Test"]);
		}
	}
}