package com.utils
{
	import com.rainbowcreatures.FWVideoEncoder;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.StatusEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.utils.ByteArray;
	
	public class VideoConvertionHelper
	{
		private var _container:Sprite;
		private var _fwEncoder:FWVideoEncoder;
		private var _captureArea:MovieClip;
		private var _frameIndex:int = 0;
		private var _totalFrames:int = 50;
		private var _onStartCallback:Function;
		private var _onFinishCallback:Function;
		private var _onSaveCallback:Function;
		
		public function VideoConvertionHelper(container:Sprite)
		{
			_container = container;
			_fwEncoder = FWVideoEncoder.getInstance(_container);
		}
		
		public function init(captureArea:MovieClip, totalFrames:int, onStartCallback:Function, onFinishCallback:Function):void
		{
			_captureArea = captureArea;
			_frameIndex = 0;
			_totalFrames = totalFrames;
			_onStartCallback = onStartCallback;
			_onFinishCallback = onFinishCallback;
			
			_fwEncoder.setDimensions(_captureArea.width, _captureArea.height);
			_fwEncoder.addEventListener(StatusEvent.STATUS, onStatus);
		}
		
		public function recordVideo():void
		{
			if (_fwEncoder.isRecording)
				return;
			
			_fwEncoder.load();
		}
		
		public function getRecordedVideo():ByteArray
		{
			var bytes:ByteArray = _fwEncoder.getVideo();
			/*var fileName:String = "video/video.mp4";
			var file:File = File.applicationStorageDirectory.resolvePath(fileName);
			var stream:FileStream = new FileStream();
			stream.open(file, FileMode.READ);
			stream.readBytes(bytes, 0, stream.bytesAvailable);
			stream.close();*/
			
			return bytes;
		}
		
		private function onStatus(e:StatusEvent):void
		{
			trace("VideoConvertionHelper - onStatus: ", e.code);
			if (e.code == "ready") {
				// FlashyWrappers ready, capture in non-realtime mode(realtime set to false), with no audio
				//_fwEncoder.setCaptureRectangle(_captureArea.x, _captureArea.y, _captureArea.width, _captureArea.height);
				_fwEncoder.start(30);
			}
			if (e.code == "started") {
				_onStartCallback.call();
				
				_container.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			}
			if (e.code == "encoded") {
				_onFinishCallback.call();
				
				if (_container.hasEventListener(Event.ENTER_FRAME))
					_container.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			}
			if (e.code == "gallery_saved") {
				if (_onSaveCallback)
					_onSaveCallback.call();
			}
		}
		
		private function onEnterFrame(e:Event):void {
			// capture the whole stage every frame - you can also capture individual DisplayOjects
			_fwEncoder.capture();
			_frameIndex++;
			
			if (_frameIndex >= _totalFrames - 1)
			{
				// we've had enough of that, let's finish up!
				if (_container.hasEventListener(Event.ENTER_FRAME))
					_container.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
				
				if (_fwEncoder.isRecording)
					_fwEncoder.finish();
			}
		}
		
		public function saveVideoToGallery(onSaveCallback:Function):void
		{
			_onSaveCallback = onSaveCallback;
			
			if (_fwEncoder.platform == FWVideoEncoder.PLATFORM_IOS || _fwEncoder.platform == FWVideoEncoder.PLATFORM_ANDROID)
			{
				_fwEncoder.saveToGallery();
			}
		}
		
		public function saveVideoToFile(onSaveCallback:Function):void
		{
			_onSaveCallback = onSaveCallback;
			
			var fileName:String = "videos/video.mp4";
			var file:File = File.applicationStorageDirectory.resolvePath(fileName);
			var bytes:ByteArray = _fwEncoder.getVideo();
			var stream:FileStream = new FileStream();
			stream.open(file, FileMode.WRITE);
			stream.writeBytes(bytes, 0, bytes.length);
			stream.close();
			
			_onSaveCallback(fileName);
		}
	}
}