package com.utils
{
	import flash.events.Event;
	import flash.events.SampleDataEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.media.AudioPlaybackMode;
	import flash.media.Microphone;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.utils.ByteArray;
	
	public class AudioRecordHelper
	{
		private var _mic:Microphone;
		private var _soundData:ByteArray;
		private var _saveToFile:Boolean;
		
		public function AudioRecordHelper()
		{
			SoundMixer.useSpeakerphoneForVoice = true;
			SoundMixer.audioPlaybackMode = AudioPlaybackMode.MEDIA;
			
			_mic = Microphone.getMicrophone(Microphone.names[0]);
		}
		
		public function startRecording():void
		{
			if (Microphone.isSupported)
			{
				_soundData = new ByteArray();
				
				_mic = Microphone.getMicrophone(Microphone.names[0]);
				_mic.rate = 44;
				_mic.gain = 100;
				_mic.setSilenceLevel(0, 4000);
				_mic.addEventListener(SampleDataEvent.SAMPLE_DATA, onSampleDataWrite);
			}
			else
			{
				trace("No microphone support!");
			}
		}
		
		public function stopRecording():void
		{
			_mic.setLoopBack(false);
			_mic.removeEventListener(SampleDataEvent.SAMPLE_DATA, onSampleDataWrite);
		}
		
		public function playRecording(data:ByteArray = null):void
		{
			if (data)
				_soundData = data;
			
			if (!_soundData)
				return;
		
			_soundData.position = 0;
			var sound:Sound= new Sound();
			sound.addEventListener(SampleDataEvent.SAMPLE_DATA, onSampleDataRead);
			var channel:SoundChannel = sound.play();
			channel.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);
		}
		
		public function getRecording():ByteArray
		{
			return _soundData;
		}
		
		private function onSampleDataWrite(e:SampleDataEvent):void
		{
			while (e.data.bytesAvailable)
			{
				var sample:Number = e.data.readFloat();
				_soundData.writeFloat(sample);
			}
		}
		
		private function onSampleDataRead(e:SampleDataEvent):void
		{
			if (!_soundData.bytesAvailable > 0)
			{
				return;
			}
			
			for (var i:int = 0; i < 8192; i++)
			{
				var sample:Number = 0;
				if (_soundData.bytesAvailable > 0)
				{
					sample = _soundData.readFloat();
				}
				e.data.writeFloat(sample); 
				e.data.writeFloat(sample);  
			}
		}
		
		private function onSoundComplete(e:Event):void
		{
			trace("onSoundComplete");
		}
		
		private function saveRecording(bytes:ByteArray):void
		{
			var fileName:String = "recordings/recording.wav";
			var file:File = File.documentsDirectory.resolvePath(fileName);
			var stream:FileStream = new FileStream();
			stream.open(file, FileMode.WRITE);
			stream.writeBytes(bytes, 0, bytes.length);
			stream.close();
		}
		
		private function getRecording():ByteArray
		{
			var bytes:ByteArray = new ByteArray();
			var fileName:String = "recordings/recording.wav";
			var file:File = File.documentsDirectory.resolvePath(fileName);
			var stream:FileStream = new FileStream();
			stream.open(file, FileMode.READ);
			stream.readBytes(bytes, 0, stream.bytesAvailable);
			stream.close();
			
			return bytes;
		}
	}
}